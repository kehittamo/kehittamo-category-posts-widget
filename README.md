# Kehittämö Category Posts Widget #

Display posts by Category in Widget

# Setup#

After activation, enable plugin styles if needed "/wp-admin/options-general.php?page=kehittamo-category-posts-admin"

# Version history #
* 1.1.0 Added horizontal and vertical selection
* 1.0.0 Initialize (from old adrian plugin)