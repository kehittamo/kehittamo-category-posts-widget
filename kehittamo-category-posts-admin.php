<?php

namespace Kehittamo\Plugins\Category_Posts;


class SettingsPage
{
    /**
     * Holds the values to be used in the fields callbacks
     */
    private $options;

    /**
     * Start up
     */
    public function __construct()
    {

        add_action( 'admin_menu', array( $this, 'add_plugin_page' ) );
        add_action( 'admin_init', array( $this, 'page_init' ) );
    }

    /**
     * Add options page
     */
    public function add_plugin_page()
    {

        // This page will be under "Settings"
        add_options_page(
            __('Category Posts', 'kehittamo-category-posts'),
            __('Category Posts', 'kehittamo-category-posts'),
            'manage_options',
            'kehittamo-category-posts-admin',
            array( $this, 'create_admin_page' )
        );
    }

    /**
     * Options page callback
     */
    public function create_admin_page()
    {
        // Set class property
        $this->options = get_option( 'kehittamo_category_posts_settings' );
        ?>
        <div class="wrap">
            <?php screen_icon(); ?>
            <h2><?php _e('Category Posts Settings', 'kehittamo-category-posts') ?></h2>
            <form method="post" action="options.php">
            <?php
                // This prints out all hidden setting fields
                settings_fields( 'kehittamo_category_posts_settings_group' );
                do_settings_sections( 'kehittamo-category-posts-admin' );
                submit_button();
            ?>
            </form>
        </div>
        <?php
    }

    /**
     * Register and add settings
     */
    public function page_init()
    {
        register_setting(
            'kehittamo_category_posts_settings_group', // Option group
            'kehittamo_category_posts_settings', // Option name
            array( $this, 'sanitize' ) // Sanitize
        );

        add_settings_section(
            'kehittamo_category_posts_default', // ID
            __('General settings', 'kehittamo-category-posts'), // Title
            array( $this, 'print_section_info' ), // Callback
            'kehittamo-category-posts-admin' // Page
        );

        add_settings_field(
            'global_styles', // ID
              __('Use Global Styles', 'kehittamo-category-posts'), // Title
            array( $this, 'global_styles_callback' ), // Callback
            'kehittamo-category-posts-admin', // Page
            'kehittamo_category_posts_default' // Section
        );

				add_settings_field(
						'default_image_url', // ID
						__('Add default image url', 'kehittamo-category-posts'), // Title
						array( $this, 'default_image_url_callback' ), // Callback
						'kehittamo-category-posts-admin', // Page
						'kehittamo_category_posts_default' // Section
				);
    }

    /**
     * Sanitize each setting field as needed
     *
     * @param array $input Contains all settings fields as array keys
     */
    public function sanitize( $input )
    {
        $new_input = array();
        if( isset( $input['global_styles'] ) )
            $new_input['global_styles'] = absint( $input['global_styles'] );

				if ( !filter_var( esc_url($input['default_image_url']), FILTER_VALIDATE_URL) === false) {
					$new_input['default_image_url'] = $input['default_image_url'];
				}

        return $new_input;
    }

    /**
     * Print the Section text
     */
    public function print_section_info()
    {
    }

    /**
     * Get the settings option array and print one of its values
     */
    public function global_styles_callback()
    {
        printf(

            '<input type="checkbox" id="global_styles" name="kehittamo_category_posts_settings[global_styles]" value="1"' . checked( 1, $this->options['global_styles'], false ) . '/>'

        );
    }

    /**
     * Get the settings option array and print one of its values
     */
    public function default_image_url_callback()
    {
        printf(
            '<input type="text" id="default_image_url" name="kehittamo_category_posts_settings[default_image_url]" value="' . $this->options['default_image_url'] . '"/>'
        );
    }
}

if( is_admin() )
    $kehittamo_category_posts_settings_page = new \Kehittamo\Plugins\Category_Posts\SettingsPage();
