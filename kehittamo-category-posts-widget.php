<?php
/*
  Plugin Name: Kehittämö Category Posts Widget
  Plugin URI: http://www.kehittamo.fi
  Description: Lists latest posts from selected category
  Version: 1.1.0
  Author: Kehittämö Oy
  Author URI: http://www.kehittamo.fi
  License: GPL2
*/

/*  Copyright 2014  MIKKO VIRENIUS  (email : mikko.virenius@adian.fi)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License, version 2, as
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

namespace Kehittamo\Plugins\Category_Posts;

define( 'Kehittamo\Plugins\Category_Posts\PLUGIN_PATH', plugin_dir_path( __FILE__ ) );
define( 'Kehittamo\Plugins\Category_Posts\PLUGIN_URL', plugin_dir_url( __FILE__ ) );

class load {

  /**
  * __construct function.
  *
  * @access public
  * @return void
  */
  function __construct() {

      // Load Plugin
      add_action('plugins_loaded', array( $this, 'plugins_loaded') );

  }



  /**
   * Loads the plugin
   */
  function plugins_loaded(){

    // Load Translateiton
    $this->load_textdomain();

    // Load Widgets
    add_action('widgets_init', array( $this, 'register_widgets' ) );

    // Load Javascript
    add_action('wp_enqueue_scripts', array( $this, 'js' ) );

    // Load admin scripts
    add_action('admin_enqueue_scripts', array( $this, 'admin_enqueue_scripts') );

    // Load Styles
    add_action('init', array( $this, 'styles' ) );

    $this->admin();
  }

    /**
     * Plugin admin page
     */
    function admin(){

       // Load plugin options page
      require_once( PLUGIN_PATH . '/kehittamo-category-posts-admin.php');

      // first check that $hook_suffix is appropriate for your admin page
     //wp_enqueue_style( 'wp-color-picker' );
    }

  /**
   * Load plugin textdomain.
   */
  function load_textdomain() {

    load_plugin_textdomain( 'kehittamo-category-posts-widget', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );

  }

  /**
   * Loads and registers widgets
   */
  function register_widgets(){

    // Include the widget file
    require_once( PLUGIN_PATH . '/widgets/widget.php');

    // Register the Widget
    register_widget( 'Kehittamo\Plugins\Category_Posts\Widget' );

  }

  /**
   * Loads Javascript files
   */
  function js(){
  }

  /**
   * Load Admin Javascript and Styles
   */
  function admin_enqueue_scripts(){
    if( is_admin() ) {
      // first check that $hook_suffix is appropriate for your admin page
      wp_enqueue_style( 'wp-color-picker' );
      wp_register_script( 'kehittamo-category-posts-admin-js', PLUGIN_URL . 'js/kehittamo-category-posts-admin.js', array('wp-color-picker','underscore') );
      wp_enqueue_script('kehittamo-category-posts-admin-js');
    }
  }

  /**
   * Loads CSS styles
   */
  function styles(){
    $options = get_option( 'kehittamo_category_posts_settings' );

    // Load registered style
    if( ! is_admin() AND isset( $options['global_styles'] ) AND $options['global_styles'] == 1 ) {
      // Register plugin style
      wp_register_style( 'kehittamo-category-posts-widget', PLUGIN_URL . '/css/kehittamo-category-posts-widget.css' );
      wp_enqueue_style('kehittamo-category-posts-widget');
    }
  }
}


$kehittamo_category_posts = new \Kehittamo\Plugins\Category_Posts\Load();
