<?php

namespace Kehittamo\Plugins\Category_Posts;

use \WP_Widget;
use \WP_Query;

/**
 * Arki_Widget_Category_Posts Class
 */
class Widget extends WP_Widget {

  /**
   * Register widget with WordPress.
   */
  function __construct() {
    parent::WP_Widget( 'category_posts',  __('Category Posts', 'kehittamo-category-posts-widget') );
  }

    /** @see WP_Widget::widget */
    function widget($args, $instance) {
        extract( $args );

        $position =  $instance['position'] ? $instance['position'] : '';
        $title = apply_filters('widget_title', $instance['title']);
        $selected_cats = $instance['selected_cats'] ? (array) $instance['selected_cats'] : array();
        $post_per_page =  $instance['post_per_page'] ? intval($instance['post_per_page']) : 5;
        $character_limit =  $instance['character_limit'] ? intval($instance['character_limit']) : '';
        $max_height =  $instance['max_height'] ? intval($instance['max_height']) : '';
        $max_width =  $instance['max_width'] ? $instance['max_width'] : '';
        $excerpt_length =  $instance['excerpt_length'] ? intval($instance['excerpt_length']) : '';
        $hide_post_thumbnail = esc_attr($instance['hide_post_thumbnail']);
        $color =  $instance['color'] ? $instance['color'] : '#515151';
        $bgcolor =  $instance['bgcolor'] ? $instance['bgcolor'] : '#fff';

        $thumbnail_size = 'thumbnail-sx';
        //$placeholdit_url = ($thumbnail_size === 'square' || 'circle') ? 'http://placehold.it/200x200' : 'http://placehold.it/350x200';
        $placeholdit_url = 'http://placehold.it/120x70';
        $kehittamo_options = get_option( 'kehittamo_category_posts_settings' );
        $default_image_id = $this->get_media_id_by_url( $kehittamo_options['default_image_url'] );
        if( $default_image_id ) {
          $image = wp_get_attachment_image_src( $default_image_id, 'thumbnail-lists' );
        }

        $before_widget = str_replace("class=", 'style="background-color:' . $bgcolor . ';color:'.$color.'" class=', $before_widget );
        if( $position === "horizontal") {
          $before_widget = str_replace('class="', 'class="horizontal ', $before_widget );
        } else {
          $before_widget = str_replace('class="', 'class="vertical ', $before_widget );
        }

        ?>
              <?php echo $before_widget; ?>
                  <?php if ( $title )
                        echo $before_title . $title . $after_title; ?>

                    <?php if( $position === "horizontal"): ?>
                      <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                      <ul class="flex-container">
                    <?php else: ?>
                      <ul class="widget_list" <?php if($max_height !== "" ) { echo 'style="bacground-color:#0F0;max-height:'.$max_height.'px;overflow:auto"'; } ?>>
                    <?php endif; ?>

                    <?php
                      $category_posts = new WP_Query();
                      $query = array( 'showposts' => $post_per_page, $post_type => 'post', 'category__in' => array_keys($selected_cats) );
                      $category_posts->query( $query );
                    ?>
                    <?php if( $category_posts->have_posts() ) : while ( $category_posts->have_posts() ) : $category_posts->the_post(); ?>

                      <?php if( $position === "horizontal"): ?>
                        <li class="flex-item" style="width:<?php echo $max_width; ?>">
                      <?php else: ?>
                        <li>
                      <?php endif; ?>

                      <?php
                       if( ! $hide_post_thumbnail ) {
                        echo '<a class="widget-entry-title" href="<?php the_permalink() ?>" rel="bookmark">';
                        if( has_post_thumbnail() AND ! $hide_post_thumbnail ) {
                          the_post_thumbnail('thumbnail-lists', array('class' => 'attachment-thumbnail-wide'));
                        }
                        elseif( !is_null($default_image_id)) {
                          // default image from admin page and found in database
                          echo '<picture class="'.$thumbnail_size.'">' .
                                 '<img src="'. $image[0] .'"/>'.
                              '</picture>';
                        }
                        else {
                          // default placehold.it image
                          echo '<picture class="'.$thumbnail_size.'">' .
                            '<img src="'. $placeholdit_url .'"/>'.
                            '</picture>';
                        }
                        echo '</a>';
                      }
                      ?>

                      <div class="widget-entry">
                      <div class="widget-entry-meta">
                          <span class="widget-entry-category"><?php echo $this->get_sub_category(); ?></span>
                          <span class="widget-entry-date"><?php the_time( 'j.n.Y g:H' ); ?></span>
                        </div>
                        <a class="widget-entry-title" href="<?php the_permalink() ?>" rel="bookmark"><?php echo $this->trim( get_the_title(), $character_limit ); ?></a>
                        <?php if( $excerpt_length > 0 ) : ?>
                          <p class="widget-entry-content">
                            <?php echo $this->trim( get_the_excerpt(), $excerpt_length );?>
                          </p>
                        <?php endif; ?>
                      </div>
                      <div class="clear"></div>
                    </li>
                    <?php endwhile; endif;  ?>
                    <?php wp_reset_query(); ?>
                  </ul>
                  <?php if( $position === "horizontal"): ?>
                    </div>
                    </div>
                  <?php endif; ?>

              <?php echo $after_widget; ?>
        <?php
    }

    /** @see WP_Widget::update */
    function update($new_instance, $old_instance) {
      $instance = $old_instance;
      $instance['title'] = strip_tags($new_instance['title']);
      $instance['position'] = $new_instance['position'];
      $instance['selected_cats'] = $new_instance['selected_cats'];
      $instance['character_limit'] = ($new_instance['character_limit']) ? intval($new_instance['character_limit']) : '';
      $instance['max_height'] = ($new_instance['max_height']) ? intval($new_instance['max_height']) : '';
      $instance['max_width'] = ($new_instance['max_width']) ? $new_instance['max_width'] : '';
      $instance['excerpt_length'] = ($new_instance['excerpt_length']) ? intval($new_instance['excerpt_length']) : '';
      $instance['post_per_page'] = intval($new_instance['post_per_page']);
      $instance['hide_post_thumbnail'] = strip_tags( $new_instance['hide_post_thumbnail'] );
      $instance['category_id'] = $new_instance['category_id'];
      $instance['color'] = ($new_instance['color']) ? $new_instance['color'] : '';
      $instance['bgcolor'] = ($new_instance['bgcolor']) ? $new_instance['bgcolor'] : '';

      return $instance;

    }

    /** @see WP_Widget::form */
    function form($instance) {
      $instance       = wp_parse_args( (array) $instance, array( 'title' => '' ) );
      $title          = esc_attr($instance['title']);
      $position       = $instance['position'] ? $instance['position'] : 'vertical';
      $selected_cats  = $instance['selected_cats'] ? (array) $instance['selected_cats'] : array();
      $post_per_page  = $instance['post_per_page'] ? intval($instance['post_per_page']) : 5;
      $character_limit =$instance['character_limit'] ? intval($instance['character_limit']) : '';
      $max_height     = $instance['max_height'] ? intval($instance['max_height']) : '';
      $max_width      = $instance['max_width'] ? $instance['max_width'] : '';
      $excerpt_length = $instance['excerpt_length'] ? intval($instance['excerpt_length']) : '';
      $color          = $instance['color'] ? $instance['color'] : '#515151';
      $bgcolor        = $instance['bgcolor'] ? $instance['bgcolor'] : '#fff';
      ?>

      <p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:', 'kehittamo-category-posts-widget'); ?> <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" /></label></p>

      <p><label for="<?php echo $this->get_field_id('character_limit'); ?>"><?php _e('Character limit:', 'kehittamo-category-posts-widget'); ?> <input class="widefat" id="<?php echo $this->get_field_id('character_limit'); ?>" name="<?php echo $this->get_field_name('character_limit'); ?>" type="text" value="<?php echo $character_limit; ?>" /></label></p>

      <p>
        <label for="<?php echo $this->get_field_id('positions'); ?>"><?php _e('Position:', 'kehittamo-category-posts-widget'); ?></label><br />

        <label for="<?php echo $this->get_field_id('position'); ?>">
          <?php _e('Vertical', 'kehittamo-category-posts-widget'); ?>
          <input class="" id="<?php echo $this->get_field_id('position_1'); ?>" name="<?php echo $this->get_field_name('position'); ?>" type="radio" value="vertical" <?php if($position === 'vertical'){ echo 'checked="checked"'; } ?> />
        </label>
        <label for="<?php echo $this->get_field_id('position'); ?>">
          <?php _e('Horizontal', 'kehittamo-category-posts-widget'); ?>
          <input class="" id="<?php echo $this->get_field_id('position_2'); ?>" name="<?php echo $this->get_field_name('position'); ?>" type="radio" value="horizontal" <?php if($position === 'horizontal'){ echo 'checked="checked"'; } ?> />
        </label>
      </p>
      <p id="max_height"><label for="<?php echo $this->get_field_id('max_height'); ?>"><?php _e('Set max height:', 'kehittamo-category-posts-widget'); ?> <input class="widefat" id="<?php echo $this->get_field_id('max_height'); ?>" name="<?php echo $this->get_field_name('max_height'); ?>" type="text" value="<?php echo $max_height; ?>" /></label></p>
      <p id="max_width"><label for="<?php echo $this->get_field_id('max_width'); ?>"><?php _e('Set width:', 'kehittamo-category-posts-widget'); ?> (px/%) <input class="widefat" id="<?php echo $this->get_field_id('max_width'); ?>" name="<?php echo $this->get_field_name('max_width'); ?>" type="text" value="<?php echo $max_width; ?>" /></label></p>

      <strong><?php _e('Select categories', 'kehittamo-category-posts-widget'); ?></strong>
      <?php  $categories = get_categories( array('hide_empty'=> 1) );
      foreach($categories as $category) : $id = $category->cat_ID; ?>
      <p>
        <input type="checkbox" name="<?php echo $this->get_field_name('selected_cats'); ?>[<?php echo $category->cat_ID; ?>]" <?php checked( $selected_cats[$id] , 'on'); ?> />
        <label for="<?php echo $this->get_field_id('selected_cats'); ?>"><?php echo $category->cat_name; ?></label>
      </p>
      <?php endforeach; ?>

      <p><label for="<?php echo $this->get_field_id('post_per_page'); ?>"><?php _e('How many articles to show?:', 'kehittamo-category-posts-widget'); ?> <input class="widefat" id="<?php echo $this->get_field_id('post_per_page'); ?>" name="<?php echo $this->get_field_name('post_per_page'); ?>" type="text" value="<?php echo $post_per_page; ?>" /></label></p>
      <p>
        <input class="checkbox" type="checkbox" <?php checked($instance['hide_post_thumbnail'], 'on'); ?> id="<?php echo $this->get_field_id('hide_post_thumbnail'); ?>" name="<?php echo $this->get_field_name('hide_post_thumbnail'); ?>" />
        <label for="<?php echo $this->get_field_id('hide_post_thumbnail'); ?>"><?php _e( 'Hide posts thumbnail', 'kehittamo-category-posts-widget' ); ?></label>
      </p>

      <p><label for="<?php echo $this->get_field_id('excerpt_length'); ?>"><?php _e('Excerpt length:', 'kehittamo-category-posts-widget'); ?> <input class="widefat" id="<?php echo $this->get_field_id('excerpt_length'); ?>" name="<?php echo $this->get_field_name('excerpt_length'); ?>" type="text" value="<?php echo $excerpt_length; ?>" /></label></p>

      <p>
        <label for="<?php echo $this->get_field_id( 'color' ); ?>"><?php _e( 'Text Color:', 'kehittamo-category-posts-widget' ); ?></label><br />
        <input class="color-picker widefat" id="<?php echo $this->get_field_id( 'color' ); ?>" name="<?php echo $this->get_field_name( 'color' ); ?>" type="text" value="<?php echo esc_attr( $color ); ?>" data-default-color="#515151">
      </p>

      <p>
        <label for="<?php echo $this->get_field_id( 'bgcolor' ); ?>"><?php _e( 'Background Color:', 'kehittamo-category-posts-widget' ); ?></label><br />
        <input class="color-picker widefat" id="<?php echo $this->get_field_id( 'bgcolor' ); ?>" name="<?php echo $this->get_field_name( 'bgcolor' ); ?>" type="text" value="<?php echo esc_attr( $bgcolor ); ?>" data-default-color="#ffffff">
      </p>

      <script>
        jQuery(document).ready(function($) {
          var rdio = '<?php echo $this->get_field_name('position'); ?>';
          jQuery('[name="' +rdio +'"]').click(function (e) {
            if(e.currentTarget.value==="vertical") {
              jQuery('p#max_height').show();
              jQuery('p#max_width').hide();
            } else {
              jQuery('p#max_height').hide();
              jQuery('p#max_width').show();
            }
          });
          jQuery('[name="' +rdio +'"]').click();
        });
      </script>

    <?php
    }

    function get_sub_category(){

      if( get_the_category() ){
        foreach(( get_the_category() ) as $category) {
          if( $category->parent ){
            return $category->name; break;
          }
        }
      }
      else{
        return false;
      }
    }

    function trim( $string = '', $length = '' ){

      if( ! $string )
        return false;

      if( $length > 0 AND mb_strlen( $string ) >= $length ){
        $output = mb_substr( $string, 0, $length ) . '...';
      } else {
        $output = get_the_title();
      }
      return $output;

    }


    private function get_media_id_by_url( $url = "" ) {
      // Split the $url into two parts with the wp-content directory as the separator
      $parsed_url  = explode( parse_url( WP_CONTENT_URL, PHP_URL_PATH ), $url );
      // Get the host of the current site and the host of the $url, ignoring www
      $this_host = str_ireplace( 'www.', '', parse_url( home_url(), PHP_URL_HOST ) );
      $file_host = str_ireplace( 'www.', '', parse_url( $url, PHP_URL_HOST ) );
      // Return nothing if there aren't any $url parts or if the current host and $url host do not match
      if ( ! isset( $parsed_url[1] ) || empty( $parsed_url[1] ) || ( $this_host != $file_host ) ) {
        return;
      }
      // Now we're going to quickly search the DB for any attachment GUID with a partial path match
      // Example: /uploads/2013/05/test-image.jpg
      $ffile = "/wp-content" . $parsed_url[1];
      global $wpdb;
      $attachment = $wpdb->get_row( $wpdb->prepare( "SELECT * FROM $wpdb->posts WHERE guid = '" . $ffile . "'" ) );
      // Returns null if no attachment is found
      return $attachment->ID;
    }

} // class Arki_Widget_Category_Posts
