/**
 * Thanks to Thomas Griffin for his super useful example on Github
 *
 * https://github.com/thomasgriffin/New-Media-Image-Uploader
 */

function initColorPicker(widget) {
  widget.find('.color-picker').wpColorPicker({
    change: _.throttle(function() { // For Customizer
      jQuery(this).trigger('change');
    }, 3000)
  });
}

function onFormUpdate(event, widget) {
  initColorPicker(widget);
}

jQuery(document).on('widget-added widget-updated', onFormUpdate);

jQuery(document).ready(function($) {
  $('#widgets-right .widget:has(.color-picker)').each(function() {
    initColorPicker($(this));
  });
});
